# Plugin Check Factory

Le plugin Check Factory a pour objectif de fournir des objets et des API génériques de gestion de contrôles et de leurs
observations et de faciliter la construction de dashboards de suivi au travers d’une interface utilisateur dans l’espace privé.

La documentation est consulatble sur SPIP-Contrib : https://contrib.spip.net/Check-Factory