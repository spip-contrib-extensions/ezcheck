<?php
/**
 * Ce fichier contient les fonctions de création, de mise à jour et de suppression
 * du schéma de données propres au plugin (tables et configuration).
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Installation du schéma de données propre au plugin et gestion des migrations suivant
 * les évolutions du schéma.
 *
 * Le schéma comprend des tables et des variables de configuration.
 *
 * @param string $nom_meta_base_version Nom de la meta dans laquelle sera rangée la version du schéma
 * @param string $version_cible         Version du schéma de données en fin d'upgrade
 *
 * @return void
 */
function ezcheck_upgrade(string $nom_meta_base_version, string $version_cible) : void {
	$maj = [];

	// Création des tables
	$maj['create'] = [
		['maj_tables', ['spip_types_controles', 'spip_controles', 'spip_observations']],
	];

	$maj['2'] = [
		['sql_alter', "TABLE spip_types_controles ADD fonction varchar(255) DEFAULT '' NOT NULL AFTER include"],
		['sql_alter', "TABLE spip_types_controles ADD squelette varchar(255) DEFAULT '' NOT NULL BEFORE actif"],
		['sql_alter', "TABLE spip_types_controles ADD contexte text DEFAULT '' NOT NULL BEFORE actif"],
		['sql_alter', "TABLE spip_types_controles CHANGE actions anomalies text DEFAULT '' NOT NULL"],
	];

	$maj['3'] = [
		['sql_alter', "TABLE spip_types_controles ADD necessite text DEFAULT '' NOT NULL AFTER icone"],
	];

	$maj['4'] = [
		['sql_alter', 'TABLE spip_controles ADD maj timestamp DEFAULT current_timestamp ON UPDATE current_timestamp'],
	];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Suppression de l'ensemble du schéma de données propre au plugin, c'est-à-dire
 * les tables et les variables de configuration.
 *
 * @param string $nom_meta_base_version Nom de la meta dans laquelle sera rangée la version du schéma
 *
 * @return void
 */
function ezcheck_vider_tables(string $nom_meta_base_version) : void {
	// On efface les tables des contrôles et anomalies
	sql_drop_table('spip_types_controles');
	sql_drop_table('spip_controles');
	sql_drop_table('spip_observations');

	// on efface la meta du schéma du plugin
	effacer_meta($nom_meta_base_version);
}
