<?php
/**
 * Ce fichier contient les fonctions d'instanciation des pipelines utiles au plugin.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ajouter du contenu au centre de la page sur les pages privées.
 *
 * Page d'adminisration des plugins :
 * - on recharge les types de contrôle.
 * - on recharge les dashboards
 *
 * @uses type_controle_charger()
 * @uses dashboard_charger()
 *
 * @param array $flux Flux d'entrée contenant la chaine affichée
 *
 * @return array Flux complétée par Check Factory
 */
function ezcheck_affiche_milieu(array $flux) : array {
	if (isset($flux['args']['exec'])) {
		// Initialisation de la page du privé
		$exec = $flux['args']['exec'];

		if ($exec === 'admin_plugin') {
			// Administration des plugins

			// On recharge les types de contrôles dont la liste a pu changer.
			include_spip('inc/ezcheck_type_controle');
			type_controle_charger();

			// On recharge les dashboards dont la liste et le contenu ont pu changer.
			include_spip('inc/ezcheck_dashboard');
			dashboard_charger();
		}
	}

	return $flux;
}
