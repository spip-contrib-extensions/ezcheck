<?php
/**
 * Ce fichier contient les fonctions déclaration des tables nécessaires au plugin.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Déclaration des nouvelles tables de la base de données propres au plugin et ne correspondant pas à un objet.
 *
 * Le plugin déclare une nouvelle table de ce type :
 * - `spip_types_controles`, qui contient les éléments descriptifs des types de contrôles disponibles
 *
 * @pipeline declarer_tables_principales
 *
 * @param array $tables_principales Tableau global décrivant la structure des tables de la base de données
 *
 * @return array Tableau fourni en entrée et mis à jour avec les nouvelles déclarations
 */
function ezcheck_declarer_tables_principales(array $tables_principales) : array {
	// Table spip_types_noisettes
	$types_controles = [
		'identifiant' => "varchar(255) DEFAULT '' NOT NULL",  // Identifiant du type de contrôle
		'nom'         => "text DEFAULT '' NOT NULL",          // Nom littéral du contrôle
		'description' => "text DEFAULT '' NOT NULL",          // Description du contrôle
		'icone'       => "varchar(255) DEFAULT '' NOT NULL",  // Fichier image sans chemin représentatif du type de contrôle
		'necessite'   => "text DEFAULT '' NOT NULL",          // Liste des préfixes des plugins nécessités par le type de contrôle
		'est_etat'    => "varchar(3) DEFAULT 'non' NOT NULL", // Indique si le contrôle est un état (uniquement un squelette pas de fonction PHP)
		'include'     => "varchar(255) DEFAULT '' NOT NULL",  // Chemin relatif du fichier contenant la fonction de contrôle
		'fonction'    => "varchar(255) DEFAULT '' NOT NULL",  // Nom de la fonction d'exécution (défaut = identifiant du type de contrôle)
		'parametres'  => "text DEFAULT '' NOT NULL",          // Paramètres à fournir en entrée de la fonction et/ou du squelette sous forme de saisies
		'anomalies'   => "text DEFAULT '' NOT NULL",          // Liste des identifiants d'anomalies rangée suivant les actions acquitter ou corriger et chemin de l'include des fonctions de correction
		'squelette'   => "varchar(255) DEFAULT '' NOT NULL",  // Chemin relatif du squelette HTML permettant un affichage complémentaire spécifique au contrôle
		'contexte'    => "text DEFAULT '' NOT NULL",          // Tableau de contexte spécifique au squelette
		'actif'       => "varchar(3) DEFAULT 'oui' NOT NULL", // Indicateur d'activité du contrôle. Si 'non', aucun contrôle de ce type ne peut être réalisé. Permet aussi de remonter des erreurs (nok, nof)
		'signature'   => "varchar(32) DEFAULT '' NOT NULL",   // MD5 du fichier de configuration du contrôle
		'maj'         => 'timestamp DEFAULT current_timestamp ON UPDATE current_timestamp',
	];

	$types_controles_cles = [
		'PRIMARY KEY' => 'identifiant',
		'KEY actif'   => 'actif',
	];

	$tables_principales['spip_types_controles'] = [
		'field' => &$types_controles,
		'key'   => &$types_controles_cles,
	];

	return $tables_principales;
}

/**
 * Déclaration des objets nécessaires au plugin.
 * Le plugin ajoute :
 * - l'objet contrôle qui correspond à une fonction lancée périodiquement ou à la demande. Un contrôle est une instance
 *   d'un type de contrôle.
 * - l'objet observation, qui résulte des contrôles.
 *
 * @pipeline declarer_tables_objets_sql
 *
 * @param array $tables_objet_sql Description des tables de la base.
 *
 * @return array Description des tables de la base complétée par celles du plugin.
 */
function ezcheck_declarer_tables_objets_sql(array $tables_objet_sql) : array {
	// Table spip_controles, description des contrôles manuels ou périodiques, instances d'un type de contrôle.
	$tables_objet_sql['spip_controles'] = [
		'type'       => 'controle',
		'principale' => 'oui',
		'field'      => [
			'id_controle'    => 'bigint(21) NOT NULL',
			'type_controle'  => "varchar(255) DEFAULT '' NOT NULL",                // Type de contrôle réalisé
			'parametres'     => "text DEFAULT '' NOT NULL",                        // Paramètres valorisés d'exécution du contrôle (saisies)
			'date'           => "datetime DEFAULT '0000-00-00 00:00:00' NOT NULL", // Date de l'activation
			'etat_execution' => "varchar(32) DEFAULT 'exec_ok' NOT NULL",          // Statut retourné en fin d'exécution : 'exec_ok', 'exec_nok_xxx'
			'id_auteur'      => 'bigint(21) NOT NULL',                             // Si activation 'user' id de l'admin sinon 0
			'nb_anomalies'   => 'smallint DEFAULT 0 NOT NULL',                     // Nombre d'anomalies ouvertes
			'maj'            => 'timestamp DEFAULT current_timestamp ON UPDATE current_timestamp',
		],
		'key' => [
			'PRIMARY KEY'       => 'id_controle',
			'KEY type_controle' => 'type_controle',
		],
		'titre' => '',

		// Edition, affichage et recherche
		'page'              => '',
		'url_voir'          => '',
		'editable'          => 'non',
		'champs_editables'  => [],
		'champs_versionnes' => [],
		'rechercher_champs' => [],
		'tables_jointures'  => [],

		// Textes standard
		'texte_retour'          => '',
		'texte_modifier'        => '',
		'texte_creer'           => '',
		'texte_creer_associer'  => '',
		'texte_signale_edition' => '',
		'texte_objet'           => 'controle:titre_controle',
		'texte_objets'          => 'controle:titre_controles',
		'info_aucun_objet'      => 'controle:info_aucun_controle',
		'info_1_objet'          => 'controle:info_1_controle',
		'info_nb_objets'        => 'controle:info_nb_controle',
		'texte_logo_objet'      => '',
	];

	// Table spip_observations, les résultats des contrôles (anomalies ou logs)
	$tables_objet_sql['spip_observations'] = [
		'type'       => 'observation',
		'principale' => 'oui',
		// Déclaration des champs
		'field' => [
			'id_observation' => 'bigint(21) NOT NULL',
			'id_controle'    => 'bigint(21) NOT NULL',                     // Id du contrôle ayant détecté l'anomalie
			'type_controle'  => "varchar(255) DEFAULT '' NOT NULL",        // Type de contrôle pour simplifier les boucles
			'objet'          => "varchar(255) NOT NULL default ''",        // Type d'objet spip ou nom qui sera affiché pour l'objet qui n'est pas forcément un objet spip
			'id_objet'       => 'bigint(21) NOT NULL default 0',           // Id de l'objet sur lequel porte l'anomalie si c'est un objet spip : permet de calculer l'url
			'url_objet'      => "varchar(255) DEFAULT '' NOT NULL",        // Url de l'objet de l'anomalie dans le cas où ce n'est pas un objet spip
			'est_anomalie'   => "varchar(3) DEFAULT 'oui' NOT NULL",       // Indicateur d'anomalie 'oui' ou 'non'
			'gravite'        => "varchar(1) DEFAULT 'e' NOT NULL",         // Gravité de l'anomalie : 'e' pour erreur, 'a' pour avertissement et 'i' pour info
			'code'           => "varchar(127) DEFAULT '' NOT NULL",        // Identifiant relatif d'une observation
			'statut'         => "varchar(10) DEFAULT 'publie' NOT NULL",   // Statut de l'anomalie : 'ouverte', 'fermee', 'poubelle'
			'parametres'     => "text DEFAULT '' NOT NULL",                // Paramètres permettant d'expliquer l'observation
			'date'           => "datetime DEFAULT '0000-00-00 00:00:00' NOT NULL", // Date correspondant au statut courant
			'maj'            => 'timestamp DEFAULT current_timestamp ON UPDATE current_timestamp',
		],
		'key' => [
			'PRIMARY KEY'      => 'id_observation',
			'KEY id_controle'  => 'id_controle',
			'KEY objet'        => 'objet',
			'KEY id_objet'     => 'id_objet',
			'KEY est_anomalie' => 'est_anomalie',
			'KEY code'         => 'code',
		],
		'join' => [
			'id_observation' => 'id_observation',
			'id_controle'    => 'id_controle',
		],
		'titre' => '',

		// Edition, affichage et recherche
		'page'              => '',
		'url_voir'          => '',
		'editable'          => 'non',
		'champs_editables'  => [],
		'champs_versionnes' => [],
		'rechercher_champs' => [],
		'tables_jointures'  => [],

		// Statuts
		'statut' => [
			[
				'champ'     => 'statut',
				'publie'    => 'ouverte',
				'previsu'   => '',
				'exception' => ['statut', 'tout']
			]
		],
		'statut_titres' => [
			'ouverte'  => 'observation:titre_observation_ouverte',
			'fermee'   => 'observation:titre_observation_fermee',
			'poubelle' => 'observation:titre_observation_poubelle'
		],
		'statut_images' => [
			'ouverte'  => 'puce-proposer-8.png',
			'fermee'   => 'puce-publier-8.png',
			'poubelle' => 'puce-supprimer-8.png',
		],
		'statut_textes_instituer' => [
			'ouverte'  => 'observation:texte_observation_ouverte',
			'fermee'   => 'observation:texte_observation_fermee',
			'poubelle' => 'observation:texte_observation_poubelle',
		],
		'texte_changer_statut' => '',

		// Textes standard
		'texte_retour'          => '',
		'texte_modifier'        => '',
		'texte_creer'           => '',
		'texte_creer_associer'  => '',
		'texte_signale_edition' => '',
		'texte_objet'           => 'observation:titre_observation',
		'texte_objets'          => 'observation:titre_observations',
		'info_aucun_objet'      => 'observation:info_aucun_observation',
		'info_1_objet'          => 'observation:info_1_observation',
		'info_nb_objets'        => 'observation:info_nb_observation',
		'texte_logo_objet'      => '',
	];

	return $tables_objet_sql;
}

/**
 * Déclaration des informations tierces (alias, traitements, jointures, etc)
 * sur les tables de la base de données modifiées ou ajoutées par le plugin.
 *
 * Le plugin se contente de déclarer les alias des tables et quelques traitements.
 *
 * @pipeline declarer_tables_interfaces
 *
 * @param array $interface Tableau global des informations tierces sur les tables de la base de données
 *
 * @return array Tableau fourni en entrée et mis à jour avec les nouvelles informations
 */
function ezcheck_declarer_tables_interfaces(array $interface) : array {
	// Les tables : permet d'appeler une boucle avec le *type* de la table uniquement
	$interface['table_des_tables']['types_controles'] = 'types_controles';
	$interface['table_des_tables']['controles'] = 'controles';
	$interface['table_des_tables']['observations'] = 'observations';

	// Les traitements
	// - table spip_anomalies : on desérialise les tableaux
	$interface['table_des_traitements']['PARAMETRES']['observations'] = 'unserialize(%s)';
	// - table spip_controles : on desérialise les tableaux
	$interface['table_des_traitements']['PARAMETRES']['controles'] = 'unserialize(%s)';
	// - table spip_types_controles : on passe typo pour afficher le nom et la description et on désérialise
	$interface['table_des_traitements']['NOM']['types_controles'] = 'typo(%s)';
	$interface['table_des_traitements']['DESCRIPTION']['types_controles'] = 'typo(%s)';
	$interface['table_des_traitements']['NECESSITE']['types_controles'] = 'unserialize(%s)';
	$interface['table_des_traitements']['PARAMETRES']['types_controles'] = 'unserialize(%s)';
	$interface['table_des_traitements']['ANOMALIES']['types_controles'] = 'unserialize(%s)';
	$interface['table_des_traitements']['CONTEXTE']['types_controles'] = 'unserialize(%s)';

	return $interface;
}
