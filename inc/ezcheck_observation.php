<?php
/**
 * Ce fichier contient l'API de gestion des observations.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ajoute une observation à la table `spip_observations`.
 *
 * @api
 *
 * @uses objet_lire()
 * @uses objet_inserer()
 * @uses objet_modifier()
 *
 * @param bool       $est_anomalie Indique si l'observation est une anomalie ou pas
 * @param int        $id_controle  Identifiant numérique du contrôle
 * @param null|array $valeurs      Tableau des valeurs de certains champs de l'observation
 *
 * @return int Identifiant numérique de l'observation crée ou 0 si erreur.
 */
function observation_ajouter(bool $est_anomalie, int $id_controle, ?array $valeurs = []) : int {
	// Lecture pour le contrôle concerné de son type et du nombre d'anomalies ouvertes.
	// - on utilise le forcage de lecture pour by-passer le cache statique de la fonction objet_lire() car sinon
	//   le nombre d'anomalies mis à jour lors de l'ajout de l'observation ne serait pas à jour.
	include_spip('action/editer_objet');
	$controle = objet_lire(
		'controle',
		$id_controle,
		[
			'champs' => ['type_controle', 'nb_anomalies'],
			'force'  => true
		]
	);

	// Initialisation par défaut des champs de l'observation
	$observation = [
		'id_controle'   => $id_controle,
		'type_controle' => $controle['type_controle'],
		'objet'         => '',
		'id_objet'      => 0,
		'url_objet'     => '',
		'est_anomalie'  => $est_anomalie ? 'oui' : 'non',
		'gravite'       => $est_anomalie ? 'e' : 'i',
		'code'          => '',
		'statut'        => $est_anomalie ? 'ouverte' : 'fermee',
		'date'          => date('Y-m-d H:i:s'),
		'parametres'    => []
	];

	// Prise en compte des champs fournis par l'appelant.
	$observation = array_merge($observation, array_intersect_key($valeurs, $observation));

	// Sérialisation des paramètres
	$observation['parametres'] = serialize($observation['parametres']);

	// Insertion d'une nouvelle obeservation.
	if ($id_observation = objet_inserer('observation', null, $observation)) {
		if ($est_anomalie) {
			// On incrémente le nombre d'anomalies ouvertes du contrôle si on vient d'en rajouter une.
			$maj_controle = ['nb_anomalies' => $controle['nb_anomalies'] + 1];
			objet_modifier('controle', $id_controle, $maj_controle);
		}
	} else {
		$id_observation = 0;
	}

	return $id_observation;
}

/**
 * Renvoie l'information brute demandée pour l'ensemble des observations ou toute les descriptions si aucun champ
 * n'est explicitement demandé.
 * Il est possible de filtrer la liste des observations.
 *
 * @api
 *
 * @param null|array<string, mixed> $filtres Tableau associatif `[champ] = valeur` ou `[champ] = !valeur` de critères de filtres sur les
 *                                           champs chaine des types de contrôle. Les opérateurs égalité et inégalité sont possibles.
 * @param null|string               $champ   Identifiant d'un champ de la description d'un contrôle.
 *                                           Si l'argument est vide, la fonction renvoie les descriptions complètes et si l'argument est
 *                                           un champ invalide la fonction renvoie un tableau vide.
 *
 * @return array Tableau de la forme `[id_observation] = champ ou description complète`. Les champs textuels
 *               et les tableaux sérialisés sont retournés en l'état.
 */
function observation_repertorier(?array $filtres = [], ?string $champ = '') : array {
	// On calcule le where à partir des filtres sachant que tous les champs sont des chaines.
	$where = [];
	if ($filtres) {
		foreach ($filtres as $_champ => $_critere) {
			$operateur = '=';
			$valeur = $_critere;
			if (substr($_critere, 0, 1) === '!') {
				$operateur = '!=';
				$valeur = ltrim($_critere, '!');
			}
			$where[] = $_champ . $operateur . sql_quote($valeur);
		}
	}

	// On récupère tous les champs des types de contrôle.
	if ($observations = sql_allfetsel('*', 'spip_observations', $where)) {
		if ($champ and $observations[0][$champ]) {
			// On demande un seul champ et ce champ est bien valide
			$retour = array_column($observations, $champ, 'id_observation');
		} else {
			// Tous les champs sauf le timestamp 'maj' sont renvoyés.
			$retour = array_column($observations, null, 'id_observation');
		}
	} else {
		$retour = [];
	}

	return $retour;
}

/**
 * Effectue les traitements adéquats pour clore une observation : acquitte une anomalie, corrige une anomalie ou
 * supprime une observation.
 * Acquitter et supprimer sont des actions qui ne requièrent que la mise à jour du statut. L'action correction elle
 * nécessite l'appel à la fonction de correction configurée.
 *
 * @api
 *
 * @uses objet_lire()
 * @uses type_controle_lire()
 * @uses objet_modifier()
 *
 * @param string $action         Identifiant de l'action à appliquer à l'observation. Prend les valeurs `corriger`,
 *                               `acquitter` ou `supprimer`.
 * @param int    $id_observation Identifiant numérique de l'observation.
 * @param int    $id_auteur      Id de l'auteur ou 0 sinon.
 *
 * @return bool
 */
function observation_cloturer(string $action, int $id_observation, int $id_auteur) : bool {
	// Initialisation du retour de la fonction
	$retour = true;

	// Récupération des informations nécessaires sur l'observation
	include_spip('action/editer_objet');
	$observation = objet_lire('observation', $id_observation);
	$id_controle = $observation['id_controle'];

	// Récupération des informations nécessaires sur le contrôle :
	// -- on force la relecture complète car cette fonction peut être appelée plusieurs fois dans le même hit et que
	//    le nombre d'anomalies du contrôle peut être décrémenté.
	$controle = objet_lire('controle', $id_controle, ['force' => true]);

	// Récupération des informations nécessaires sur le type de contrôle
	include_spip('inc/ezcheck_type_controle');
	$type_controle = type_controle_lire($controle['type_controle']);

	// Traitements pre changement de statut
	$pre_traitement_ok = true;
	if ($action === 'corriger') {
		// Exécuter la fonction de correction configurée qui existe forcément (sinon le contrôle n'est pas exécutable)
		include_spip($type_controle['anomalies']['include']);
		$corriger = "{$controle['type_controle']}_{$observation['code']}";
		// La fonction de correction doit retourner '' si ok et un identifiant d'erreur non vide sinon.
		if ($erreur = $corriger($id_observation, $id_auteur)) {
			$pre_traitement_ok = false;
			spip_log("Fonction de correction : `{$corriger}` en erreur ({$erreur})", 'ezcheck' . _LOG_ERREUR);
		}
	}

	if ($pre_traitement_ok) {
		// Initialisation du statut final en fonction de l'action.
		$maj_observation = [
			'statut' => ($action === 'supprimer' ? 'poubelle' : 'fermee'),
		];

		// Changement de statut
		if ($erreur = objet_modifier('observation', $id_observation, $maj_observation)) {
			$retour = false;
			spip_log("Modification de l'observation `{$id_observation}` en erreur ({$erreur})", 'ezcheck' . _LOG_ERREUR);
		} elseif (
			($action !== 'supprimer')
			and ($observation['est_anomalie'] === 'oui')
		) {
			// Traitements post changement de statut :
			// - il faut décrémenter le nombre d'anomalies ouvertes du contrôle associé.
			$maj_controle = [
				'nb_anomalies' => $controle['nb_anomalies'] - 1
			];
			objet_modifier('controle', $id_controle, $maj_controle);
		}
	} else {
		$retour = false;
	}

	return $retour;
}
