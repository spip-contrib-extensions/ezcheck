<?php
/**
 * Ce fichier contient l'API de gestion des dashboards.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Charge ou recharge la configuration des dashboards à partir de leur fichier YAML ou JSON et du pipeline `eztypecontrole_declarer`.
 * La fonction compile les dashboards dans un cache unique sécurisé.
 * Etant donné que le nombre de dashboards est en général assez faible, l'ensemble du cache est systématiquement
 * recalculé.
 *
 * @api
 *
 * @uses yaml_decode_file()
 * @uses cache_ecrire()
 *
 * @return bool `false` si une erreur s'est produite, `true` sinon.
 */
function dashboard_charger() : bool {
	// Retour de la fonction
	$retour = true;

	// On recherche les dashboards:
	// -- par leur fichier YAML ou JSON de configuration. La recherche s'effectue dans le path en utilisant le dossier
	//    relatif fourni.
	$dashboards = [];
	$fichiers = find_all_in_path('ezcheck/dashboards/', '.+[.](yaml|json)$');
	foreach ($fichiers as $_nom_fichier => $_chemin) {
		// Détermination de l'extension du fichier json ou yaml.
		$extension = pathinfo($_nom_fichier, PATHINFO_EXTENSION);
		// L'identifiant du dashboard est son nom de fichier sans extension
		$id_dashboard = basename($_nom_fichier, ".{$extension}");

		// on constitue un tableau unifié dont les clés sont les identifiants de dashboard
		$dashboards[$id_dashboard] = [
			'est_fichier' => true,
			'extension'   => $extension,
			'chemin'      => $_chemin,
		];
	}

	// -- par l'intermédiaire d'un pipeline qui permet à des plugins de fournir leurs propres type de contrôle. Le
	//    tableau est au même format que le fichier YAML ou JSON décodé.
	//    Attention, si on utilise le même identifiant dans le pipeline, le dashboard fichier disparaitra.
	$flux = [
		'args' => [],
		'data' => $dashboards
	];
	$dashboards = pipeline('eztypecontrole_declarer', $flux);

	// On recherche les dashboards directement par leur fichier YAML de configuration car il est
	// obligatoire. La recherche s'effectue dans le path en utilisant le dossier relatif fourni.
	if ($dashboards) {
		foreach ($dashboards as $_id_dashboard => $_description) {
			// Initialisation de la description par défaut du type de contrôle
			$description_defaut = [
				'identifiant' => $_id_dashboard,
				'nom'         => $_id_dashboard,
				'description' => '',
				'icone'       => 'dashboard_defaut-24.svg',
				'groupes'     => [],
			];

			// Si le dashboard est issu d'un fichier de configuration YAML ou JSON on le lit et le décode en structure
			// de données PHP.
			if (!empty($_description['est_fichier'])) {
				if ($_description['extension'] === 'json') {
					include_spip('inc/flock');
					lire_fichier($_description['chemin'], $dashboard_contenu);
					$description = json_decode($dashboard_contenu, true);
				} else {
					include_spip('inc/yaml');
					$description = yaml_decode_file($_description['chemin'], ['include' => false]);
				}
			} else {
				$description = $_description;
			}

			// Complétude de la description avec les valeurs par défaut
			$description = array_merge($description_defaut, $description);

			// On reformate le tableau des groupes pour que l'index soit l'identifiant
			$groupes = [];
			foreach ($description['groupes'] as $_groupe) {
				$groupes[$_groupe['identifiant']] = $_groupe;
			}
			$description['groupes'] = $groupes;

			// Permettre à des plugins de compléter la description du dashboard, en particulier, d'ajouter
			// des groupes et/ou des types de contrôle à un groupe.
			$flux = [
				'args' => [
					'dashboard' => $_id_dashboard,
				],
				'data' => $description
			];
			$description = pipeline('ezdashboard_declarer', $flux);

			// On ajoute le dashboard nouveau ou modifié
			$dashboards[$_id_dashboard] = $description;
		}

		// Etant donné que le nombre dashboard est réduit tout comme les informations qui le compose on choisit
		// de l'écrire systématiquement.
		// -- Initialisation de l'identifiant du cache des dashboards
		$cache = [
			'nom' => 'dashboards',
		];
		// -- Ecriture du cache
		include_spip('inc/ezcache_cache');
		cache_ecrire('ezcheck', 'dashboard', $cache, $dashboards);
	}

	return $retour;
}

/**
 * Retourne la description complète du dashboard.
 * Les champs textuels peuvent subir un traitement typo si demandé.
 *
 * @api
 *
 * @uses dashboard_repertorier()
 *
 * @param string    $id_dashboard Identifiant du dashboard.
 * @param null|bool $traiter_typo Indique si les données textuelles doivent être retournées brutes ou si elles doivent
 *                                être traitées en utilisant la fonction typo.
 *
 * @return array<string, mixed> La description complète. Si demandé, les champs textuels peuvent être traités avec la fonction typo().
 */
function dashboard_lire(string $id_dashboard, ?bool $traiter_typo = false) : array {
	// On met en cache mémoire statique les dashboards déjà lus.
	static $dashboards_charges = [];

	// On vérifie si le dashboard demandé n'est pas déjà stocké : si oui, la description est renvoyée immédiatement
	if (isset($dashboards_charges[$traiter_typo][$id_dashboard])) {
		$dashboard = $dashboards_charges[$traiter_typo][$id_dashboard];
	} else {
		// Il faut charger le dashboard depuis le cache. On initialise avec le tableau qui est indique une erreur.
		$dashboard = [];

		// Chargement de tous les dashboards connus à partir du fichier cache.
		$dashboards_caches = dashboard_repertorier();

		// Sauvegarde de la description de la page pour une consultation ultérieure dans le même hit.
		if (isset($dashboards_caches[$id_dashboard])) {
			// Extraction du dashboard demandé
			$dashboard = $dashboards_caches[$id_dashboard];

			// Traitements des champs textuels
			if ($traiter_typo) {
				$dashboard['nom'] = typo($dashboard['nom']);
				if ($dashboard['description']) {
					$dashboard['description'] = typo($dashboard['description']);
				}
				foreach ($dashboard['groupes'] as $_id => $_groupe) {
					$dashboard['groupes'][$_id]['nom'] = typo($dashboard['groupes'][$_id]['nom']);
				}
			}

			// Stockage de la description du dashboard lu
			$dashboards_charges[$traiter_typo][$id_dashboard] = $dashboard;
		} else {
			// En cas d'erreur stocker un dashboard vide
			$dashboards_charges[$traiter_typo][$id_dashboard] = [];
		}
	}

	return $dashboard;
}

/**
 * Renvoie l'information brute demandée pour l'ensemble des dashboards utilisés
 * ou toute les descriptions si aucune information n'est explicitement demandée.
 *
 * @api
 *
 * @uses cache_lire()
 *
 * @param null|string $information Identifiant d'un champ de la description d'un dashboard.
 *                                 Si l'argument est vide, la fonction renvoie les descriptions complètes et si l'argument est
 *                                 un champ invalide la fonction renvoie un tableau vide.
 *
 * @return array<string, mixed> Tableau de la forme `[dashboard] = information ou description complète`. Les champs textuels
 *                              sont retournés en l'état, le timestamp `maj` n'est pas fourni.
 */
function dashboard_repertorier(?string $information = '') : array {
	// Initialiser le tableau de sortie en cas d'erreur
	$dashboards = [];

	// Les dashboards sont stockées dans un cache sécurisé géré par Cache Factory.
	// -- Initialisation de l'identifiant du cache des dashboards
	$cache = [
		'nom' => 'dashboards',
	];

	include_spip('inc/ezcache_cache');
	if ($descriptions = cache_lire('ezcheck', 'dashboard', $cache)) {
		if ($information) {
			// Si $information n'est pas une colonne valide array_column retournera un tableau vide.
			if ($informations = array_column($descriptions, $information, 'identifiant')) {
				$dashboards = $informations;
			}
		} else {
			$dashboards = $descriptions;
		}
	}

	return $dashboards;
}

/**
 * Renvoie la configuration complète d’un dashboard ainsi que des informations supplémentaires provenant
 * de l'environnement de la page dashboard (groupe et type de contrôle à afficher par défaut).
 *
 * @api
 *
 * @uses dashboard_lire()
 *
 * @param string      $id_dashboard  Identifiant du dashboard
 * @param null|string $id_groupe     Identifiant du groupe affiché par défaut ou vide pour afficher le premier
 * @param null|string $type_controle Identifiant du type de contrôle affiché par défaut ou vide pour afficher le premier
 *
 * @return array<string, mixed> Tableau du contexte contenant la description du dashboard
 */
function dashboard_contextualiser(string $id_dashboard, ?string $id_groupe = '', ?string $type_controle = '') : array {
	// On initialise le contexte à vide en cas d'erreur
	$contexte = [];

	// On récupère la description du dashboard désigné par son identifiant
	include_spip('inc/ezcheck_dashboard');
	if ($id_dashboard) {
		$contexte = dashboard_lire($id_dashboard);

		// On rajoute le groupe à afficher ou à défaut le premier
		if (isset($contexte['groupes'][$id_groupe])) {
			$contexte['groupe_id_defaut'] = $id_groupe;
		} else {
			$groupe = reset($contexte['groupes']);
			$contexte['groupe_id_defaut'] = $groupe['identifiant'];
		}

		// Maintenant qu'on a le groupe on rajoute le type de controle à afficher ou à défaut le premier du groupe
		if (in_array($type_controle, $contexte['groupes'][$contexte['groupe_id_defaut']]['controles'])) {
			$contexte['type_controle_defaut'] = $type_controle;
		} else {
			$contexte['type_controle_defaut'] = reset($contexte['groupes'][$contexte['groupe_id_defaut']]['controles']);
		}
	}

	return $contexte;
}
