<?php
/**
 * Ce fichier contient l'API de gestion des types de contrôle.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Charge ou recharge les descriptions des types de contrôle à partir des fichiers YAML et du pipeline
 * `eztypecontrole_declarer`.
 * La fonction optimise le chargement en effectuant uniquement les traitements nécessaires en fonction des modifications,
 * ajouts et suppressions des contrôles identifiés en comparant les md5 des descriptions.
 *
 * @api
 *
 * @uses type_controle_repertorier()
 *
 * @param null|bool $recharger Si `true` force le rechargement de tous les types de contrôles sinon le chargement se base
 *                             sur le md5 des fichiers YAML. Par défaut vaut `false`.
 *
 * @return bool `false` si une erreur s'est produite, `true` sinon.
 */
function type_controle_charger(?bool $recharger = false) : bool {
	// Retour de la fonction
	$retour = true;

	// On recherche les types de contrôle:
	// -- par leur fichier YAML de configuration. La recherche s'effectue dans le path en utilisant le dossier
	//    relatif fourni.
	$types_controle = [];
	$fichiers = find_all_in_path('ezcheck/controles/', '.+[.]yaml$');
	foreach ($fichiers as $_nom_fichier => $_chemin) {
		// L'identifiant du type de contrôle est son nom de fichier sans extension
		$id_type_controle = basename($_nom_fichier, '.yaml');

		// on constitue un tableau unifié dont les clés sont les identifiants de type de contrôle
		$types_controle[$id_type_controle] = [
			'est_fichier' => true,
			'chemin'      => $_chemin,
		];
	}

	// -- par l'intermédiaire d'un pipeline qui permet à des plugins de fournir leurs propres type de contrôle. Le
	//    tableau est au même format que le fichier YAML décodé.
	//    Attention, si on utilise le même identifiant dans le pipeline, le type de contrôle fichier disparaitra.
	$flux = [
		'args' => [],
		'data' => $types_controle
	];
	$types_controle = pipeline('eztypecontrole_declarer', $flux);

	if ($types_controle) {
		// Initialisation des tableaux de types de contrôle.
		$types_controle_a_ajouter = $types_controle_a_changer = $types_controle_a_effacer = [];

		// Récupération de la description complète des contrôles déjà enregistrés de façon :
		// - à gérer l'activité des types en fin de chargement
		// - de comparer les signatures md5 des noisettes déjà enregistrées. Si on force le rechargement il est inutile
		//   de gérer les signatures et les contrôles modifiés ou obsolètes.
		$types_controle_existants = type_controle_repertorier();
		$signatures = [];
		if (!$recharger) {
			$signatures = array_column($types_controle_existants, 'signature', 'identifiant');
			// On initialise la liste des contrôles à supprimer avec l'ensemble des contrôles déjà stockés.
			$types_controle_a_effacer = $signatures ? array_keys($signatures) : [];
		}

		foreach ($types_controle as $id_type_controle => $_description) {
			// Si on a forcé le rechargement ou si aucun md5 n'est encore stocké pour le contrôle
			// on positionne la valeur du md5 stocké à chaine vide.
			// De cette façon, on force la lecture du fichier YAML du contrôle ou de la description issue du pipeline.
			$md5_stocke = (isset($signatures[$id_type_controle]) and !$recharger)
				? $signatures[$id_type_controle]
				: '';

			// Initialisation de la description par défaut du type de contrôle
			$description_defaut = [
				'identifiant' => $id_type_controle,
				'nom'         => $id_type_controle,
				'description' => '',
				'icone'       => 'controle_defaut-24.svg',
				'necessite'   => [],
				'est_etat'    => 'non',
				'include'     => '',
				'fonction'    => $id_type_controle,
				'parametres'  => [],
				'anomalies'   => [],
				'squelette'   => '',
				'contexte'    => [],
				'actif'       => 'oui',
				'signature'   => '',
			];

			// On vérifie que le md5 du fichier YAML ou du tableau issu du pipeline est bien différent de celui stocké
			// avant de charger le contenu. Sinon, on passe au type contrôle suivant.
			$md5 = (!empty($_description['est_fichier']))
				? md5_file($_description['chemin'])
				: md5(json_encode($_description));
			if ($md5 != $md5_stocke) {
				// Lecture et décodage du fichier YAML en structure de données PHP ou récupération de la structure
				// tel que si elle provient du pipeline.
				include_spip('inc/yaml');
				$description = (!empty($_description['est_fichier']))
					? yaml_decode_file($_description['chemin'], ['include' => true])
					: $_description;

				if ($description === false) {
					$description = [];
					$description['actif'] = 'noy';
				}

				// Ajout de la signature
				$description['signature'] = $md5;

				// Complétude de la description avec les valeurs par défaut
				$description = array_merge($description_defaut, $description);

				if ($description['actif'] === 'oui') {
					// Aplatir les blocs execution et affichage et normaliser les paramètres du formulaire
					if (
						isset($description['execution'])
						and ($execution = $description['execution'])
					) {
						unset($description['execution']);
						if (isset($execution['parametres'])) {
							$description['parametres']['fonction'] = $execution['parametres'];
							unset($execution['parametres']);
						}
						$description = array_merge($description, $execution);
					}
					if (
						isset($description['affichage'])
						and ($affichage = $description['affichage'])
					) {
						unset($description['affichage']);
						if (isset($affichage['parametres'])) {
							$description['parametres']['squelette'] = $affichage['parametres'];
							unset($affichage['parametres']);
						}
						$description = array_merge($description, $affichage);
					}
					$parametres_defaut = [
						'fonction'  => [],
						'squelette' => []
					];
					$description['parametres'] = array_merge($parametres_defaut, $description['parametres']);

					// Normalisation des champs relatifs à la fonction d'exécution:
					if ($description['include']) {
						// Détermination du chemin exact de l'include
						// - '/nom_fichier' indique que le fichier est dans le répertoire relatif par défaut, soit 'ezcheck/controles/'.
						// - '[dir_relatif/]nom_fichier' indique qu'il faut utiliser le chemin relatif fourni tel que.
						$nom_fichier = basename($description['include']);
						if ($description['include'] === "/{$nom_fichier}") {
							$description['include'] = "ezcheck/controles/{$nom_fichier}";
						}

						// Vérification de l'existence de la fonction PHP
						include_spip($description['include']);
						if (!function_exists($description['fonction'])) {
							$description['actif'] = 'nof';
						}
					} else {
						// On nettoie le nom de la fonction initialisé par défaut
						$description['fonction'] = '';
					}

					// Normalisation du bloc anomalies qui doit toujours contenir un tableau 'acquitter' et un autre
					// 'corriger'.
					// Les boutons en options ne sont disponibles que si des anomalies acquittables
					// ou corrigeables existent : on ne génère pas d'erreur, on force les indicateurs.
					$actions_defaut = [
						'include'   => '',
						'acquitter' => [],
						'corriger'  => [],
						'options'   => [],
					];
					$description['anomalies'] = array_merge($actions_defaut, $description['anomalies']);
					$options_defaut = [
						'acquitter_si_corriger' => false,
						'bouton_tout_acquitter' => false,
						'bouton_tout_corriger'  => false,
					];
					$description['anomalies']['options'] = array_merge($options_defaut, $description['anomalies']['options']);
					if ($description['anomalies']['corriger']) {
						// Identification de l'include dans lequel on ira chercher les actions. Il peut être différent
						// de celui de la fonction d'exécution et dans ce cas il suit la même syntaxe.
						if (!$description['anomalies']['include']) {
							// -- c'est le même include que celui de la fonction d'exécution
							$description['anomalies']['include'] = $description['include'];
						} else {
							$nom_fichier = basename($description['anomalies']['include']);
							if ($description['anomalies']['include'] === "/{$nom_fichier}") {
								$description['anomalies']['include'] = "ezcheck/controles/{$nom_fichier}";
							}
						}

						// Vérification de l'existence de la fonction PHP de correction si la configuration du type de contrôle
						// le demande
						include_spip($description['anomalies']['include']);
						foreach ($description['anomalies']['corriger'] as $_correction) {
							if (!function_exists("{$id_type_controle}_{$_correction}")) {
								$description['actif'] = 'noc';
								break;
							} elseif ($description['anomalies']['options']['acquitter_si_corriger']) {
								$description['anomalies']['acquitter'][] = $_correction;
							}
						}
					} else {
						$description['anomalies']['options']['bouton_tout_corriger'] = false;
						$description['anomalies']['options']['acquitter_si_corriger'] = false;
					}
					if (!$description['anomalies']['acquitter']) {
						$description['anomalies']['options']['bouton_tout_acquitter'] = false;
						$description['anomalies']['options']['acquitter_si_corriger'] = false;
					}
					// S'assurer que les listes corriger et acquitter ne possèdent pas de doublon
					$description['anomalies']['corriger'] = array_unique($description['anomalies']['corriger']);
					$description['anomalies']['acquitter'] = array_unique($description['anomalies']['acquitter']);

					// Traitement du squelette d'affichage complémentaire:
					// -- on détermine le chemin exact du squelette
					$squelette = "ezcheck/controles/{$id_type_controle}";
					if ($description['squelette']) {
						$squelette = $description['squelette'];
						$nom_fichier = basename($description['squelette']);
						if ($description['squelette'] === "/{$nom_fichier}") {
							$squelette = "ezcheck/controles/{$nom_fichier}";
						}
					}
					// -- on vérifie ensuite que ce squelette existe.
					//    Si il n'existe pas c'est soit qu'il est inutilisé (contrôle sans affichage complémentaire),
					//    soit une erreur.
					//    Si il existe et qu'il n'a pas de paramètres c'est un état.
					if (find_in_path("{$squelette}.html")) {
						$description['squelette'] = $squelette;
						if (
							!$description['include']
							and !$description['parametres']['squelette']
						) {
							$description['est_etat'] = 'oui';
						}
					} elseif ($description['squelette']) {
						$description['actif'] = 'not';
					} elseif (!$description['include']) {
						$description['actif'] = 'nok';
					}

					// Si le type de contrôle est correctement configuré (donc actif), on vérifie que les éventuels plugins
					// qu'il nécessite sont bien activés. Si non, on indique le type de contrôle noisette comme inactif
					// En effet, il est inutile de déterminer l'absence de plugin nécessité si le type de contrôle n'est
					// pas bien configuré
					if (
						($description['actif'] === 'oui')
						and !empty($description['necessite'])
					) {
						foreach ($description['necessite'] as $_plugin_necessite) {
							if (!defined('_DIR_PLUGIN_' . strtoupper($_plugin_necessite))) {
								$description['actif'] = 'nop';
								break;
							}
						}
					}
				}

				// Traitement des index liés aux fonctions de détection et de correction si ils existent :
				// - on sérialise les champs 'necessite', 'parametres' et 'anomalies' qui sont des tableaux
				$description['necessite'] = serialize($description['necessite']);
				$description['parametres'] = serialize($description['parametres']);
				$description['anomalies'] = serialize($description['anomalies']);
				$description['contexte'] = serialize($description['contexte']);

				if (!$md5_stocke or $recharger) {
					// Le type de noisette est soit nouveau soit on est en mode rechargement forcé:
					// => il faut le rajouter.
					$types_controle_a_ajouter[] = $description;
				} else {
					// La description stockée a été modifiée et le mode ne force pas le rechargement:
					// => il faut mettre à jour le type de noisette.
					$types_controle_a_changer[] = $description;
					// => et il faut donc le supprimer de la liste de types de noisette obsolètes
					$types_controle_a_effacer = array_diff($types_controle_a_effacer, [$id_type_controle]);
				}
			} else {
				// Le type de noisette n'a pas changé et n'a donc pas été rechargé:
				// => Il faut donc juste indiquer qu'il n'est pas obsolète.
				$types_controle_a_effacer = array_diff($types_controle_a_effacer, [$id_type_controle]);
			}
		}

		// Mise à jour des contrôles en base de données :
		// -- Suppression des contrôles obsolètes ou de tous les contrôles si on est en mode rechargement forcé.
		// -- Update des contrôles modifiés.
		// -- Insertion des nouveaux contrôles.

		// Mise à jour de la table des contrôles
		$from = 'spip_types_controles';
		// -- Suppression des pages obsolètes ou de toute les pages non virtuelles si on est en mode
		//    rechargement forcé.
		if (sql_preferer_transaction()) {
			sql_demarrer_transaction();
		}
		if ($types_controle_a_effacer) {
			sql_delete($from, sql_in('identifiant', $types_controle_a_effacer));
		} elseif ($recharger) {
			sql_delete($from);
		}
		// -- Update des contrôels modifiés
		if ($types_controle_a_changer) {
			sql_replace_multi($from, $types_controle_a_changer);
		}
		// -- Insertion des nouveaux contrôles
		if ($types_controle_a_ajouter) {
			sql_insertq_multi($from, $types_controle_a_ajouter);
		}
		if (sql_preferer_transaction()) {
			sql_terminer_transaction();
		}
	}

	return $retour;
}

/**
 * Retourne la description complète ou une liste de champs d'un type de contrôle donné.
 * Les tableaux sont systématiquement désérialisés.
 *
 * @api
 *
 * @param string     $id_type_controle La valeur de l'identifiant du type de contrôle.
 * @param null|array $champs           Tableau d'un champ ou de plusieurs champs de la description d'un type de contrôle.
 *                                     Si l'argument est vide, la fonction renvoie la description complète.
 *
 * @return array|bool|int|string La description brute complète ou partielle du type de contrôle :
 *                               - `false`          : l'objet demandé n'existe pas
 *                               - `array vide`     : l'objet existe, mais aucun champ demandé n'existe
 *                               - `array n champs` : tableau avec les champs demandés si ils existent sinon tableau vide
 *                               - `array 1 champ`  : valeur du champ demandé si il existe sinon `false`
 */
function type_controle_lire(string $id_type_controle, ?array $champs = []) {
	// Initialisation des tableaux statiques
	static $types_controle = [];

	if (!isset($types_controle[$id_type_controle])) {
		// Condition sur l'identifiant
		$where = [
			'identifiant=' . sql_quote($id_type_controle)
		];

		// Acquisition de tous les champs du type de contrôle et sauvegarde.
		if ($types_controle[$id_type_controle] = sql_fetsel('*', 'spip_types_controles', $where)) {
			// Traitements des champs tableaux sérialisés
			$types_controle[$id_type_controle]['necessite'] = unserialize($types_controle[$id_type_controle]['necessite']);
			$types_controle[$id_type_controle]['parametres'] = unserialize($types_controle[$id_type_controle]['parametres']);
			$types_controle[$id_type_controle]['anomalies'] = unserialize($types_controle[$id_type_controle]['anomalies']);
			$types_controle[$id_type_controle]['contexte'] = unserialize($types_controle[$id_type_controle]['contexte']);
		} else {
			// L'objet n'existe pas, on renvoie false pour le préciser.
			$types_controle[$id_type_controle] = false;
		}
	}

	// On extrait la description correspondant à l'identifiant
	$retour = $types_controle[$id_type_controle];

	if ($retour and !empty($champs)) {
		// Extraction des seules informations demandées.
		// -- si on demande une information unique on renvoie la valeur simple, sinon on renvoie un tableau.
		// -- si une information n'est pas un champ valide elle n'est pas renvoyée sans renvoyer d'erreur.
		if (count($champs) > 1) {
			// Tableau des informations valides
			$retour = array_intersect_key($retour, array_flip($champs));
		} else {
			// Valeur unique demandée.
			$retour = ($retour[array_shift($champs)] ?? false);
		}
	}

	return $retour;
}

/**
 * Renvoie l'information brute demandée pour l'ensemble des contrôles utilisés
 * ou toutes les descriptions si aucune information n'est explicitement demandée.
 *
 * @api
 *
 * @param null|array<string, mixed> $filtres Tableau associatif `[champ] = valeur` ou `[champ] = !valeur` de critères de filtres sur les
 *                                           champs chaine des types de contrôle. Les opérateurs égalité et inégalité sont possibles.
 * @param null|string               $champ   Identifiant d'un champ de la description d'un contrôle.
 *                                           Si l'argument est vide, la fonction renvoie les descriptions complètes et si l'argument est
 *                                           un champ invalide la fonction renvoie un tableau vide.
 *
 * @return array<string, mixed> Tableau de la forme `[identifiant] = champ ou description complète`. Les champs textuels
 *                              et les tableaux sérialisés sont retournés en l'état.
 */
function type_controle_repertorier(?array $filtres = [], ?string $champ = '') : array {
	// On calcule le where à partir des filtres sachant que tous les champs sont des chaines.
	$where = [];
	if ($filtres) {
		foreach ($filtres as $_champ => $_critere) {
			$operateur = '=';
			$valeur = $_critere;
			if (substr($_critere, 0, 1) === '!') {
				$operateur = '!=';
				$valeur = ltrim($_critere, '!');
			}
			$where[] = $_champ . $operateur . sql_quote($valeur);
		}
	}

	// On récupère tous les champs des types de contrôle.
	if ($types_controle = sql_allfetsel('*', 'spip_types_controles', $where)) {
		if ($champ and $types_controle[0][$champ]) {
			// On demande un seul champ et ce champ est bien valide
			$retour = array_column($types_controle, $champ, 'identifiant');
		} else {
			// Tous les champs sauf le timestamp 'maj' sont renvoyés.
			$retour = array_column($types_controle, null, 'identifiant');
		}
	} else {
		$retour = [];
	}

	return $retour;
}

/**
 * Exécute une fonction d'un type de contrôle donné.
 * Si la fonction n'existe pas c'est que l'exécution ne sert qu'à enregistrer les paramètres du formulaire
 * servant uniquement à l'affichage d'un squelette (ce n'est pas un état). Dans ce cas l'exécution ne fait que créer
 * un contrôle en pass thru.
 *
 * @api
 *
 * @uses objet_lire()
 * @uses type_controle_lire()
 * @uses objet_modifier()
 *
 * @param string               $id_type_controle Id tu type de contrôle
 * @param int                  $id_auteur        Id de l'auteur ayant lancé le contrôle (ou 0 pour le CRON)
 * @param array<string, mixed> $options          Tableau des arguments valorisés de la fonction et/ou du squelette. Peut être vide, sinon
 *                                               possède deux index `fonction` et `squelette`. Seul l'index `fonction` est fourni à la
 *                                               fonction d'exécution si elle existe mais les deux index sont stockés dans le contrôle.
 *
 * @return bool `false` si une erreur s'est produite, `true` sinon.
 */
function type_controle_executer(string $id_type_controle, int $id_auteur, array $options) : bool {
	// On initialise le retour de la fonction à erreur
	$retour = false;

	// On commence par créer une instance de contrôle qui recueillera l'état d'exécution et les éventuelles
	// observations.
	include_spip('action/editer_objet');
	$parametres_defaut = [
		'fonction'  => [],
		'squelette' => []
	];
	$options = array_merge($parametres_defaut, $options);
	$controle = [
		'type_controle'  => $id_type_controle,
		'parametres'     => serialize($options),
		'date'           => date('Y-m-d H:i:s'),
		'id_auteur'      => $id_auteur,
		'etat_execution' => 'exec_ok',
		'nb_anomalies'   => 0,
	];

	if ($id_controle = objet_inserer('controle', null, $controle)) {
		// Le contrôle a bien été créé, on peut exécuter la fonction de contrôle si elle existe.
		// -- lecture des informations sur le type de contrôle.
		$type_controle = type_controle_lire($id_type_controle);

		// Il existe deux types de fonctions d'exécution:
		// -- la fonction créée spécialement pour l'occasion et qui porte le nom du type : elle a forcément les
		//    arguments $id_controle, $id_auteur et $options
		// -- la fonction réutilisée d'une API de plugin dont les arguments sont imposés
		// De fait, il faut distinguer ces deux cas car l'appel et le retour sont différents
		$maj_controle = [];
		$fonction = $type_controle['fonction'];
		if ($id_type_controle === $fonction) {
			// -- la fonction de contrôle peut détecter et créer des observations et retourne une chaine vide
			//    ou un identifiant d'erreur suivant que le traitement s'est bien déroulé ou pas.
			//    Aucune information n'est fournie par la fonction sur le nombre d'anomalies détectées.
			//    A chaque insertion d'une anomalie le compteur des anomalies ouvertes du contrôle est incrémenté.
			include_spip($type_controle['include']);
			$erreur = $fonction($id_controle, $id_auteur, $options['fonction']);
			if (!$erreur) {
				// Tout s'est bien passé
				$retour = true;
			} else {
				// On met à jour l'état d'exécution du contrôle en erreur
				$maj_controle['etat_execution'] = "exec_nok_{$erreur}";
			}
		} elseif ($fonction) {
			// -- la fonction de contrôle est une API de plugin, on l'appelle avec sa liste prédéfinie d'arguments.
			//    Le retour est toujours ok car la fonction n'a pas forcément été conçue pour retourner une erreur.
			//    C'est une limitation de la réutilisation, tout comme le fait de ne pas pouvoir générer d'observation.
			include_spip($type_controle['include']);
			$arguments = [];
			foreach ($options['fonction'] as $_parametre) {
				$arguments[] = $_parametre;
			}
			call_user_func_array($fonction, $arguments);
			$retour = true;
		} else {
			// -- l'exécution ne sert qu'à créer un contrôle qui va permettre d'enregistrer les paramètres du squelette.
			//    Le retour est toujours ok
			$retour = true;
		}

		// Si nécessaire (erreur d'exécution ou anomalies détectées) on met à jour le contrôle.
		if ($maj_controle) {
			objet_modifier('controle', $id_controle, $maj_controle);
		}
	}

	return $retour;
}

/**
 * Renvoie l'identifiant du dashboard auquel est rattaché le type contrôle.
 *
 * @api
 *
 * @uses dashboard_repertorier()
 *
 * @param string      $id_type_controle Identifiant du type de contrôle.
 * @param null|string $liaison          Type de liaison à identifier. Prend les valeurs :
 *                                      - `dashboard` pour renvoyer l'id du dashboard
 *                                      - `groupe` pour renvoyer l'id du groupe
 *
 * @return string Idenfiant du dashboard de rattachement ou chaine vide sinon.
 */
function type_controle_identifier_liaison(string $id_type_controle, ?string $liaison = 'dashboard') : string {
	// On renvoie chaine vide si pas trouvé
	$id = '';

	if ($id_type_controle) {
		// Acquérir tous les dashboards
		include_spip('inc/ezcheck_dashboard');
		$dashboards = dashboard_repertorier();

		$controle_trouve = false;
		foreach ($dashboards as $_id => $_dashboard) {
			if (isset($_dashboard['groupes'])) {
				foreach ($_dashboard['groupes'] as $_groupe) {
					if (in_array($id_type_controle, $_groupe['controles'])) {
						$id = $liaison === 'dashboard' ? $_id : $_groupe['identifiant'];
						$controle_trouve = true;
						break;
					}
				}

				if ($controle_trouve) {
					break;
				}
			}
		}
	}

	return $id;
}
