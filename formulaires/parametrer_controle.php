<?php
/**
 * Ce fichier contient les fonctions de gestion du formulaire CVT de paramétrage et d'exécution d'un contrôle.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Chargement des données du formaulaire : le formulaire permet d'afficher le descriptif du type de contrôle et
 * d'éventuellement saisir des paramètres pour lancer la fonction de vérification.
 *
 * @uses type_controle_lire()
 * @uses observation_repertorier()
 *
 * @param string      $type_controle Identifiant du type de contrôle
 * @param null|int    $id_auteur     Id de l'auteur ou 0 sinon
 * @param null|string $redirect      URL de la page à afficher suite à l'exécution du formulaire ou vide pour rester sur la même page
 *
 * @return <string, mixed> Tableau des données à charger par le formulaire (affichage et saisie) :
 *               - `_description`      : (affichage) descriptif du type de contrôle
 *               - `_message_inactif`  : (affichage) message éventuel expliquant l'inactivité du contrôle
 *               - `_champs_fonction`  : (saisie) liste des saisies pour les paramètres de la fonction de vérification
 *               - `_champs_squelette` : (saisie) liste des saisies pour les paramètres d'affichage du squelette complémentaire
 *               - `_bouton_execution` : (affichage) indique si le bouton d'exécution doit être affiché ou pas
 */
function formulaires_parametrer_controle_charger_dist(string $type_controle, ?int $id_auteur = 0, ?string $redirect = '') : array {
	// Initialisation des valeurs à passer au formulaire
	$valeurs = [
		'editable'          => false,
		'_description'      => '',
		'_message_inactif'  => false,
		'_champs_fonction'  => [],
		'_champs_squelette' => [],
		'_bouton_execution' => false
	];

	// Récupération des informations sur la type de contrôle
	include_spip('inc/ezcheck_type_controle');
	$configuration_type_controle = type_controle_lire($type_controle);
	if ($configuration_type_controle) {
		// A minima on affichera son titre, son icone et sa description
		$valeurs['_icone'] = $configuration_type_controle['icone'];
		$valeurs['_titre'] = typo($configuration_type_controle['nom']);
		$valeurs['_description'] = typo($configuration_type_controle['description']);

		// On détermine si il faut afficher le message d'inactivité
		$valeurs['_message_inactif'] = (
			($configuration_type_controle['est_etat'] === 'non')
			and ($configuration_type_controle['actif'] !== 'oui')
		);

		if (autoriser('executer', 'typecontrole', $type_controle)) {
			// Paramètres du type de contrôle défini dans son fichier YAML.
			// Cette configuration peut comporter des paramètres de saisie spécifiques dont les valeurs sont ensuite
			// stockées dans le champ 'parametres' de la table 'spip_controles'.
			// On distingue les paramètres de la fonction d'exécution et ceux du squelette.
			// Cette structure de formulaire est générée automatiquement par le plugin Saisies.
			$valeurs['_champs_fonction'] = $configuration_type_controle['parametres']['fonction'];
			$valeurs['_champs_squelette'] = $configuration_type_controle['parametres']['squelette'];

			// Déterminer le nombre d'anomalies encore ouvertes pour le type de contrôle
			include_spip('inc/ezcheck_observation');
			$filtres = [
				'type_controle' => $type_controle,
				'est_anomalie'  => 'oui',
				'statut'        => 'ouverte',
			];
			$observations = observation_repertorier($filtres, 'id_observation');
			$valeurs['_bouton_execution'] = (count($observations) === 0);

			$valeurs['editable'] = !$valeurs['_message_inactif'];
		}
	}

	return $valeurs;
}

/**
 * Vérification du formulaire : on lance les contrôles des saisies qui intègrent des vérifications.
 *
 * @uses type_controle_lire()
 * @uses saisies_verifier()
 *
 * @param string      $type_controle Identifiant du type de contrôle
 * @param null|int    $id_auteur     Id de l'auteur ou 0 sinon
 * @param null|string $redirect      URL de la page à afficher suite à l'exécution du formulaire ou vide pour rester sur la même page
 *
 * @return <string, mixed> Tableau des erreurs indexé par les noms des champs en erreur
 */
function formulaires_parametrer_controle_verifier_dist(string $type_controle, ?int $id_auteur = 0, ?string $redirect = '') : array {
	$erreurs = [];

	// Vérifier les champs correspondant aux paramètres spécifiques du type de contrôle
	include_spip('inc/ezcheck_type_controle');
	$champs = type_controle_lire($type_controle, ['parametres']);
	$champs = array_merge($champs['fonction'], $champs['squelette']);

	if ($champs) {
		include_spip('inc/saisies');
		$erreurs = saisies_verifier($champs, false);
	}

	return $erreurs;
}

/**
 * Exécution du formulaire : si le bouton d'exécution est affiché lancement de l'exécution de la vérification et affichage
 * des résultats.
 *
 * @uses type_controle_lire()
 * @uses saisies_lister_champs()
 * @uses type_controle_executer()
 *
 * @param string      $type_controle Identifiant du type de contrôle
 * @param null|int    $id_auteur     Id de l'auteur ou 0 sinon
 * @param null|string $redirect      URL de la page à afficher suite à l'exécution du formulaire ou vide pour rester sur la même page
 *
 * @return array<string, mixed> Tableau de retour contenant soit le message d'erreur soit le message de bonne exécution et l'url de
 *                              redirection éventuelle.
 */
function formulaires_parametrer_controle_traiter_dist(string $type_controle, ?int $id_auteur = 0, ?string $redirect = '') : array {
	$retour = [];

	if (autoriser('executer', 'typecontrole', $type_controle)) {
		// On constitue le tableau des valeurs des paramètres spécifiques du type de contrôle
		include_spip('inc/ezcheck_type_controle');
		$champs = type_controle_lire($type_controle, ['parametres']);

		include_spip('inc/saisies_lister');
		$parametres = [];
		if ($champs['fonction']) {
			foreach (saisies_lister_champs($champs['fonction'], false) as $_champ) {
				$parametres['fonction'][$_champ] = _request($_champ);
			}
		}
		if ($champs['squelette']) {
			foreach (saisies_lister_champs($champs['squelette'], false) as $_champ) {
				$parametres['squelette'][$_champ] = _request($_champ);
			}
		}

		// Exécution du type de contrôle avec les paramètres éventuellement saisis
		include_spip('action/executer_type_controle');
		if (type_controle_executer($type_controle, (int) $id_auteur, $parametres)) {
			$retour['redirect'] = $redirect;
		} else {
			$retour['message_erreur'] = _T('ezcheck:type_controle_execution_message_erreur');
		}
	}

	return $retour;
}
