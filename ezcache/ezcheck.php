<?php
/**
 * Ce fichier contient les fonctions de service nécessité par le plugin Check Factory.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Renvoie la configuration spécifique des caches de Check Factory.
 *
 * @param string $plugin Préfixe du plugin, à savoir, `ezcheck`.
 *
 * @return array<string, mixed> Tableau de la configuration brute du plugin Check Factory.
 */
function ezcheck_cache_configurer(string $plugin) : array {
	// Initialisation du tableau de configuration avec les valeurs par défaut du plugin Check Factory.
	$configuration = [
		'dashboard' => [
			'racine'          => '_DIR_TMP',
			'sous_dossier'    => false,
			'nom_obligatoire' => ['nom'],
			'nom_facultatif'  => [],
			'extension'       => '.php',
			'securisation'    => true,
			'serialisation'   => true,
			'decodage'        => false,
			'separateur'      => '-',
			'conservation'    => 0
		]
	];

	return $configuration;
}
