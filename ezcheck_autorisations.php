<?php
/**
 * Ce fichier contient les fonctions d'autorisations du plugin.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction appelée par le pipeline.
 */
function ezcheck_autoriser() {
}

/**
 * Autorisation minimale d'accès à toutes les pages du plugin Check Factory sauf celle de configuration
 * du plugin lui-même (controles, dashboard).
 * Par défaut, seuls les administrateurs complets sont autorisés à utiliser ces pages.
 * Cette autorisation est à la base de la plupart des autres autorisations du plugin.
 *
 * @param string         $faire   L'action : `ezcheck`
 * @param string         $type    Le type d'objet ou nom de table : chaine vide
 * @param int            $id      Id de l'objet sur lequel on veut agir : 0, inutilisé
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `null`, inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
 */
function autoriser_ezcheck_dist($faire, $type, $id, $qui, $options) {
	return autoriser('defaut');
}

/**
 * Autorisation d'affichage du menu d'accès aux dashboards (page=ezcheck).
 * Il faut être autorisé à utiliser ezcheck.
 *
 * @param string         $faire   L'action : `menu`
 * @param string         $type    Le type d'objet ou nom de table : `ezcheck` (ce n'est pas un objet au sens SPIP)
 * @param int            $id      Id de l'objet sur lequel on veut agir : 0, inutilisé
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `null`, inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
 */
function autoriser_ezcheck_menu_dist($faire, $type, $id, $qui, $options) {
	return autoriser('ezcheck');
}

/**
 * Autorisation d'accès à la page de configuration du plugin Check Factory (page=configurer_ezcheck).
 * Il faut être autorisé :
 * - à utiliser le plugin
 * - et avoir l'autorisation stabdard de configuration.
 *
 * @param string         $faire   L'action : `configurer`
 * @param string         $type    Le type d'objet ou nom de table : `ezcheck` (ce n'est pas un objet au sens SPIP)
 * @param int            $id      Id de l'objet sur lequel on veut agir : 0, inutilisé
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `null`, inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
 */
function autoriser_ezcheck_configurer_dist($faire, $type, $id, $qui, $options) {
	return
		autoriser('ezcheck')
		and autoriser('configurer');
}

/**
 * Autorisation d'afficher la page d'un dashboard donné. A partir de cette page il est possible de visualiser les
 * contrôles et les anomalies, d'exécuter un contrôle manuellement, lancer une action corrective.
 * - être autorisé à utiliser Check Factory,
 * - que l'identifiant du dashboard soit connu.
 *
 * @param string         $faire   L'action : `voir`
 * @param string         $type    Le type d'objet ou nom de table : `dashboard` (ce n'est pas un objet au sens SPIP)
 * @param int|string     $id      Id de l'objet sur lequel on veut agir : identifiant du dashboard sous forme d'une chaine.
 *                                C'est un détournement de l'utilisation habituelle pour un objet.
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `null`, inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
 */
function autoriser_dashboard_voir_dist($faire, $type, $id, $qui, $options) {
	// Initialisation de l'autorisation à non autorisé par défaut.
	$autoriser = false;

	if (autoriser('ezcheck')) {
		include_spip('inc/ezcheck_dashboard');
		$dashboard_ids = dashboard_repertorier('identifiant');
		if (
			is_string($id)
			and $id
			and in_array($id, $dashboard_ids)
		) {
			$autoriser = true;
		}
	}

	return $autoriser;
}

/**
 * Autorisation d'activation et de désactivation des types de contrôle.
 * Il faut :
 * - être autorisé à configurer Check Factory
 * - que le type de contrôle soit bien chargé
 * - et qu'il ne soit pas un état.
 *
 * @param string         $faire   L'action : `modifier`
 * @param string         $type    Le type d'objet ou nom de table : `typecontrole` (ce n'est pas un objet au sens SPIP)
 * @param int|string     $id      Id de l'objet sur lequel on veut agir : identifiant du type de contrôle sous forme d'une chaine.
 *                                C'est un détournement de l'utilisation habituelle pour un objet.
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `null`, inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
 */
function autoriser_typecontrole_modifier_dist($faire, $type, $id, $qui, $options) {
	// Initialisation de l'autorisation à non autorisé par défaut.
	$autoriser = false;

	if (autoriser('ezcheck', 'configurer')) {
		include_spip('inc/ezcheck_type_controle');
		$type_controle = type_controle_lire($id);
		if (
			$type_controle
			and ($type_controle['est_etat'] === 'non')
		) {
			$autoriser = true;
		}
	}

	return $autoriser;
}

/**
 * Autorisation d'exécution d'un type de contrôle.
 * Il faut :
 * - être autorisé à utiliser Check Factory
 * - que le type de contrôle soit bien chargé, actif et que ce ne soit pas un état
 * - et que soit la fonction de contrôle existe.
 *
 * @param string         $faire   L'action : `executer`
 * @param string         $type    Le type d'objet ou nom de table : `typecontrole` (ce n'est pas un objet au sens SPIP)
 * @param int|string     $id      Id de l'objet sur lequel on veut agir : identifiant du type de contrôle sous forme d'une chaine.
 *                                C'est un détournement de l'utilisation habituelle pour un objet.
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `null`, inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
 */
function autoriser_typecontrole_executer_dist($faire, $type, $id, $qui, $options) {
	// Initialisation de l'autorisation à non autorisé par défaut.
	$autoriser = false;

	if (autoriser('ezcheck')) {
		include_spip('inc/ezcheck_type_controle');
		$type_controle = type_controle_lire($id);
		if (
			$type_controle
			and ($type_controle['est_etat'] === 'non')
			and ($type_controle['actif'] === 'oui')
			and (
				!$type_controle['fonction']
				or (
					$type_controle['include']
					and include_spip($type_controle['include'])
					and function_exists($type_controle['fonction'])
				)
			)
		) {
			$autoriser = true;
		}
	}

	return $autoriser;
}

/**
 * Autorisation de cloture ou de mise à la poubelle des observations fermées d'un type de contrôle.
 * Il faut :
 * - être autorisé à exécuter le type de contrôle.
 *
 * @param string         $faire   L'action : `cloturer`
 * @param string         $type    Le type d'objet ou nom de table : `typecontrole` (ce n'est pas un objet au sens SPIP)
 * @param int|string     $id      Id de l'objet sur lequel on veut agir : identifiant du type de contrôle sous forme d'une chaine.
 *                                C'est un détournement de l'utilisation habituelle pour un objet.
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `action`, action de cloture parmi
 *                                `acquitter`, `corriger` ou `supprimer`
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
 */
function autoriser_typecontrole_cloturer_dist($faire, $type, $id, $qui, $options) {
	// Initialisation de l'autorisation à non autorisé par défaut.
	$autoriser = false;

	if (
		autoriser('executer', 'typecontrole', $id)
		and !empty($options['action'])
		and in_array($options['action'], ['acquitter', 'corriger', 'supprimer'])
	) {
		$autoriser = true;
	}

	return $autoriser;
}

/**
 * Autorisation de changement de statut d'une observation.
 * Il faut :
 * - être autorisé à utiliser Check Factory,
 * - que l'observation existe,
 * - que l'action soit fournie et corresponde à `corriger`, `acquitter` ou `supprimer`.
 *
 * @param string         $faire   L'action : `instituer`
 * @param string         $type    Le type d'objet ou nom de table : `observation`
 * @param int|string     $id      Id de l'objet sur lequel on veut agir : identifiant numérique de l'observation.
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : index `action` qui mène au changement
 *                                de statut.
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
 */
function autoriser_observation_instituer_dist($faire, $type, $id, $qui, $options) {
	// Initialisation de l'autorisation à non autorisé par défaut.
	$autoriser = false;

	if (
		autoriser('ezcheck')
		and include_spip('action/editer_objet')
		and objet_lire('observation', (int) $id)
		and isset($options['action'])
		and in_array($options['action'], ['corriger', 'acquitter', 'supprimer'])) {
		$autoriser = true;
	}

	return $autoriser;
}
