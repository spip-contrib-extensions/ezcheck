<?php
/**
 * Ce fichier contient les filtres et les balises du plugin.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Compile la balise `#DASHBOARD` qui fournit la configuration complète d'un dashboard ou de tous les dashboards.
 * La signature de la balise est : `#DASHBOARD{dashboard_id}`.
 *
 * @balise
 *
 * @example
 *     ```
 *     #DASHBOARD{contrib}, renvoie la description complète du dashboard d'id `contrib`
 *     #DASHBOARD, renvoie la description de tous les dashboards
 *     ```
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_DASHBOARD_dist(Champ $p) : Champ {
	// Récupération des arguments de la balise.
	// -- Identifiant du dashboard uniquement.
	$dashboard_id = interprete_argument_balise(1, $p);
	$dashboard_id = isset($dashboard_id) ? str_replace('\'', '"', $dashboard_id) : '""';

	// Calcul de la balise
	$p->code = "(include_spip('inc/ezcheck_dashboard')
		? ({$dashboard_id}
			? dashboard_lire({$dashboard_id})
			: dashboard_repertorier())
		: [])";

	return $p;
}

/**
 * Compile la balise `#DASHBOARD_CONTEXTE` qui fournit la configuration complète d'un dashboard ainsi que des
 * informations supplémentaires provenant de l'environnement de la page dashboard.
 * La signature de la balise est : `#DASHBOARD_CONTEXTE{dashboard_id, groupe_id, type_controle}`.
 *
 * @balise
 *
 * @example
 *     ```
 *     #DASHBOARD_CONTEXTE{contrib, '', ''}, renvoie la description complète du dashboard d'id `contrib` ainsi que
 *     l'id du premier groupe et le premier type de contrôle listé dans le groupe.
 *     #DASHBOARD_CONTEXTE{contrib, article, ''}, renvoie la description complète du dashboard d'id `contrib` ainsi que
 *     l'id du groupe précisé et le premier type de contrôle listé dans le groupe.
 *     #DASHBOARD_CONTEXTE{contrib, 'article', 'article_prepa'}, renvoie la description complète du dashboard d'id `contrib` ainsi que
 *     l'id du groupe et du type de contrôle précisés.
 *     ```
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 */
function balise_DASHBOARD_CONTEXTE_dist(Champ $p) : Champ {
	// Récupération des arguments de la balise.
	// -- Tous les arguments sont obligatoires.
	$dashboard_id = interprete_argument_balise(1, $p);
	$dashboard_id = isset($dashboard_id) ? str_replace('\'', '"', $dashboard_id) : '""';
	$groupe_id = interprete_argument_balise(2, $p);
	$groupe_id = isset($groupe_id) ? str_replace('\'', '"', $groupe_id) : '""';
	$type_controle = interprete_argument_balise(3, $p);
	$type_controle = isset($type_controle) ? str_replace('\'', '"', $type_controle) : '""';

	// Calcul de la balise
	$p->code = "(include_spip('inc/ezcheck_dashboard')
		? dashboard_contextualiser({$dashboard_id}, {$groupe_id}, {$type_controle})
		: [])";

	return $p;
}

/**
 * Compile la balise `#TYPE_CONTROLE_DASHBOARD` qui renvoie l'identifiant du dashboard auquel le type de contrôle est
 * rattaché.
 * La signature de la balise est : `#TYPE_CONTROLE_DASHBOARD{identifiant_type_controle}`.
 *
 * @balise
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 */
function balise_TYPE_CONTROLE_DASHBOARD_dist(Champ $p) : Champ {
	// Récupération des arguments.
	// -- l'identifiant du type de contrôle unoiquement
	$type_controle = interprete_argument_balise(1, $p);
	$type_controle = isset($type_controle) ? str_replace('\'', '"', $type_controle) : '""';

	// On calcule la balise
	$p->code = "(include_spip('inc/ezcheck_type_controle')
		? type_controle_identifier_liaison({$type_controle})
		: '')";

	return $p;
}

/**
 * Compile la balise `#TYPE_CONTROLE_GROUPE` qui renvoie l'identifiant du dashboard auquel le type de contrôle est
 * rattaché.
 * La signature de la balise est : `#TYPE_CONTROLE_GROUPE{identifiant_type_controle}`.
 *
 * @balise
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 */
function balise_TYPE_CONTROLE_GROUPE_dist(Champ $p) : Champ {
	// Récupération des arguments.
	// -- l'identifiant du type de contrôle unoiquement
	$type_controle = interprete_argument_balise(1, $p);
	$type_controle = isset($type_controle) ? str_replace('\'', '"', $type_controle) : '""';

	// On calcule la balise
	$p->code = "(include_spip('inc/ezcheck_type_controle')
		? type_controle_identifier_liaison({$type_controle}, 'groupe')
		: '')";

	return $p;
}

/**
 * Construit le titre d'un contrôle.
 *
 * @param int                  $id_objet Identifiant du contrôle
 * @param array<string, mixed> $champs   Liste des champs de l'objet concerné
 *
 * @return string
 */
function generer_titre_controle(int $id_objet, array $champs) : string {
	// Le titre d'un contrôle est de la forme type_controle:id
	$titre = "{$champs['type_controle']}:{$id_objet}";

	return $titre;
}

/**
 * Construit le titre d'une observation.
 *
 * @param int                  $id_objet Identifiant de l'observation
 * @param array<string, mixed> $champs   Liste des champs de l'objet concerné
 *
 * @return string
 */
function generer_titre_observation(int $id_objet, array $champs) : string {
	// Le titre d'une observation est de la forme code:id
	$titre = "{$champs['code']}:{$champs['id_controle']}-{$id_objet}";

	return $titre;
}

/**
 * Passe la fonction `typo()` si la valeur du paramètre est un idiome, sinon ne rien faire.
 * Cette fonction est nécessaire pour passer le paramètre dans l'environnement.
 *
 * @param mixed $valeur Chaine représentant une valeur à afficher
 *
 * @return mixed Chaine à afficher
 */
function parametre_preparer_pour_env($valeur) {
	// Si la variable est un idiome spip <:modute:item:> on passe typo
	if (
		is_string($valeur)
		and (strpos($valeur, '<:') !== false)
		and preg_match('/^<:([^>]*?):>$/', $valeur)
	) {
		$valeur = typo($valeur);
	}

	return $valeur;
}
