<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/ezcheck-paquet-xml-ezcheck?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// E
	'ezcheck_description' => 'This plugin provides check and bug objects for preventive and corrective maintenance of a website’s content. It offers an API for building an administration panel to run checks, view bugs and fix them.',
	'ezcheck_slogan' => 'Easy management of checks and bugs',
];
