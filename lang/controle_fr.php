<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/ezcheck.git

return [

	// C
	'champ_date_label' => 'Exécuté le',
	'champ_etat_execution_label' => 'Etat',
	'champ_log_label' => 'Commentaire',
	'champ_nb_anomalies_label' => 'Anomalies',
	'champ_type_controle_label' => 'Type de contrôle',

	// E
	'exec_nok_defaut' => 'en erreur',
	'exec_ok' => 'ok',

	// I
	'info_1_controle' => 'Un contrôle',
	'info_aucun_controle' => 'Aucun contrôle',
	'info_controle_avec_anomalie' => 'Controles précédents avec anomalies non corrigées',
	'info_dernier_controle' => 'Dernier contrôle exécuté',
	'info_nb_controle' => '@nb@ controles',

	// T
	'titre_controle' => 'Contrôle',
	'titre_controles' => 'Contrôles',
];
