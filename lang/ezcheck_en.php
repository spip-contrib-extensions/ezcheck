<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/ezcheck-ezcheck?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// A
	'anomalie_acquitter_bouton' => 'Acknowledge all open bugs',
	'anomalie_corriger_bouton' => 'Fix all open bugs',
	'anomalie_fermee_titre' => 'List of fixed bugs',
	'anomalie_ouverte_titre' => 'List of open bugs',
	'anomalie_vider_bouton' => 'Delete fixed bugs',

	// D
	'dashboard_bouton_recharger' => 'Reload dashboards',
	'dashboard_liste_boite_info' => 'This page allows authorized users to view the list of available dashboards, update them and consult them if they have authorization.',
	'dashboard_liste_message_vide' => 'No dashboard available.',
	'dashboard_liste_titre' => 'List of available dashboards',
	'dashboard_menu' => 'Dashboards',

	// E
	'ezcheck_titre_page' => 'Check Factory',

	// O
	'observation_acquitter_bouton' => 'Acknowledge',
	'observation_corriger_bouton' => 'Fix',
	'observation_fermee_titre' => 'List of observations',
	'observation_supprimer_bouton' => 'Delete',
	'observation_vider_bouton' => 'Clean up observations',

	// T
	'type_controle_bouton_activer' => 'Enable check type',
	'type_controle_bouton_desactiver' => 'Disable check type',
	'type_controle_bouton_executer' => 'Run',
	'type_controle_bouton_recharger' => 'Reload check types',
	'type_controle_execution_message_nok' => 'Check completed with error.',
	'type_controle_fieldset_fonction' => 'Function arguments',
	'type_controle_fieldset_squelette' => 'Additional display settings',
	'type_controle_inactif_message' => 'The check type is disabled. You must enable it before you can run it again.',
	'type_controle_liste_boite_info' => 'This page allows authorized users to manage check types: enabling, disabling, updating.',
	'type_controle_liste_message_noc' => 'The PHP function for fixing an bug cannot be found.',
	'type_controle_liste_message_nod' => 'The check type is not assigned to any dashboard.',
	'type_controle_liste_message_nof' => 'The PHP function for processing the "@extra1@" check cannot be found in "@extra2@".',
	'type_controle_liste_message_nok' => 'The check type is incorrectly configured.',
	'type_controle_liste_message_nop' => 'The following plugins, required by the check type, are currently inactive: "@extra1@".',
	'type_controle_liste_message_not' => 'The "@extra1@« template to display results cannot be found.',
	'type_controle_liste_message_noy' => 'The control type YAML description contains syntax errors.',
	'type_controle_liste_message_vide' => 'No check type available. Try reloading.',
	'type_controle_liste_titre' => 'Management of check types',
	'type_controle_menu' => 'Check types',
	'type_controle_message_vide' => 'No check type available',
];
