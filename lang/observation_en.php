<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/observation-ezcheck?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// C
	'champ_code_label' => 'Type',
	'champ_controle_label' => 'Check',
	'champ_gravite_label' => 'Severity',
	'champ_objet_label' => 'Object of interest',

	// G
	'gravite_e' => 'error',
	'gravite_i' => 'info',
	'gravite_w' => 'notice',

	// I
	'info_1_observation' => '1 observation',
	'info_aucune_observation' => 'No observation',
	'info_nb_observation' => '@nb@ observations',

	// T
	'texte_observation_fermee' => 'closed',
	'texte_observation_ouverte' => 'open',
	'texte_observation_poubelle' => 'deleted',
	'titre_observation' => 'Observation',
	'titre_observation_fermee' => 'Closed observation',
	'titre_observation_ouverte' => 'Open observation',
	'titre_observation_poubelle' => 'Observation deleted',
	'titre_observations' => 'Observations',
];
