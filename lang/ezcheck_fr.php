<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/ezcheck.git

return [

	// A
	'anomalie_acquitter_bouton' => 'Acquitter les anomalies ouvertes',
	'anomalie_corriger_bouton' => 'Corriger les anomalies ouvertes',
	'anomalie_fermee_titre' => 'Liste des anomalies fermées',
	'anomalie_ouverte_titre' => 'Liste des anomalies ouvertes',
	'anomalie_vider_bouton' => 'Supprimer les anomalies fermées',

	// D
	'dashboard_bouton_recharger' => 'Recharger les dashboards',
	'dashboard_liste_boite_info' => 'Cette page permet aux utilisateurs autorisés de consulter la liste des dashboards disponibles, de les mettre à jour et de les afficher si ils possèdent l’autorisation.',
	'dashboard_liste_message_vide' => 'Aucun dashboard disponible.',
	'dashboard_liste_titre' => 'Liste des dashboards disponibles',
	'dashboard_menu' => 'Dashboards',

	// E
	'ezcheck_titre_page' => 'Check Factory',

	// O
	'observation_acquitter_bouton' => 'Acquitter',
	'observation_corriger_bouton' => 'Corriger',
	'observation_fermee_titre' => 'Liste des observations',
	'observation_supprimer_bouton' => 'Supprimer',
	'observation_vider_bouton' => 'Nettoyer les observations',

	// T
	'type_controle_bouton_activer' => 'Activer le type de contrôle',
	'type_controle_bouton_desactiver' => 'Désactiver le type de contrôle',
	'type_controle_bouton_executer' => 'Lancer l’exécution',
	'type_controle_bouton_recharger' => 'Recharger les types de contrôle',
	'type_controle_execution_message_nok' => 'L’exécution du contrôle s’est terminé en erreur.',
	'type_controle_fieldset_fonction' => 'Arguments de la fonction d’exécution',
	'type_controle_fieldset_squelette' => 'Paramètres de l’affichage complémentaire',
	'type_controle_inactif_message' => 'Le type de contrôle est inactif. Vous devez le réactiver avant de pouvoir l’exécuter à nouveau',
	'type_controle_liste_boite_info' => 'Cette page permet aux utilisateurs autorisés d’administrer les types de contrôle : activation, désactivation, mise à jour.',
	'type_controle_liste_message_noc' => 'La fonction PHP de correction d’une anomalie est introuvable.',
	'type_controle_liste_message_nod' => 'Le type de contrôle n’est affecté à aucun dashboard.',
	'type_controle_liste_message_nof' => 'La fonction PHP d’exécution du contrôle "@extra1@" est introuvable dans "@extra2@".',
	'type_controle_liste_message_nok' => 'Le type de contrôle est mal configuré.',
	'type_controle_liste_message_nop' => 'Le ou les plugins suivants, nécessités par le type de contrôle, sont actuellement désactivés : "@extra1@".',
	'type_controle_liste_message_not' => 'Le squelette d’affichage des résultats "@extra1@" est introuvable.',
	'type_controle_liste_message_noy' => 'Le YAML de description du type de contrôle comporte des erreurs de syntaxe.',
	'type_controle_liste_message_vide' => 'Aucun type de contrôle disponible. Essayez de lancer un chargement.',
	'type_controle_liste_titre' => 'Administration des types de contrôle',
	'type_controle_menu' => 'Types de controles',
	'type_controle_message_vide' => 'Aucun type de controle disponible',
];
