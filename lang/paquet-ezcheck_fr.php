<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/ezcheck.git

return [

	// E
	'ezcheck_description' => 'Ce plugin fournit les objets contrôle et anomalie permettant de mettre en place une maintenance préventive et corrective du contenu d’un site. Il offre une API pour construire un panel d’administration pour lancer les contrôles, visualiser les anomalies et les corriger.',
	'ezcheck_slogan' => 'Faciliter la gestion de contrôles et d’anomalies',
];
