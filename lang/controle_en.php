<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/controle-ezcheck?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// C
	'champ_date_label' => 'Processed on',
	'champ_etat_execution_label' => 'Status',
	'champ_log_label' => 'Comment',
	'champ_nb_anomalies_label' => 'Bugs',
	'champ_type_controle_label' => 'Check type',

	// E
	'exec_nok_defaut' => 'in error',
	'exec_ok' => 'ok',

	// I
	'info_1_controle' => '1 check',
	'info_aucun_controle' => 'No check',
	'info_controle_avec_anomalie' => 'Previous checks not fixed',
	'info_dernier_controle' => 'Last check processed',
	'info_nb_controle' => '@nb@ checks',

	// T
	'titre_controle' => 'Check',
	'titre_controles' => 'Checks',
];
