<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/ezcheck.git

return [

	// C
	'champ_code_label' => 'Type',
	'champ_controle_label' => 'Contrôle',
	'champ_gravite_label' => 'Gravité',
	'champ_objet_label' => 'Objet concerné',

	// G
	'gravite_e' => 'erreur',
	'gravite_i' => 'info',
	'gravite_w' => 'avertissement',

	// I
	'info_1_observation' => 'Une observation',
	'info_aucune_observation' => 'Aucune observation',
	'info_nb_observation' => '@nb@ observations',

	// T
	'texte_observation_fermee' => 'fermée',
	'texte_observation_ouverte' => 'ouverte',
	'texte_observation_poubelle' => 'à la poubelle',
	'titre_observation' => 'Observation',
	'titre_observation_fermee' => 'Observation fermée',
	'titre_observation_ouverte' => 'Observation ouverte',
	'titre_observation_poubelle' => 'Observation supprimée',
	'titre_observations' => 'Observations',
];
