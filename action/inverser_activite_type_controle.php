<?php
/**
 * Ce fichier contient l'action `inverser_activite_type_controle` lancée par un utilisateur pour
 * activer ou désactiver un type de contrôle de façon sécurisée.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Cette action permet à l'utilisateur d'activer ou de désactiver, de façon sécurisée,
 * un type de contrôle donné.
 *
 * Cette action est réservée aux utilisateurs pouvant modifier un type de contrôle.
 * Elle nécessite l'identifiant du type de contrôle et son indicateur d'activité.
 *
 * @param null|string $arguments Arguments de l'action ou null si l'action est appelée par une URL
 *
 * @return void
 */
function action_inverser_activite_type_controle_dist(?string $arguments = null) : void {
	// Sécurisation.
	// Arguments attendus :
	// - l'identifiant du type de contrôle
	// - l'état d'activité courant
	if (null === $arguments) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arguments = $securiser_action();
	}

	// Récupération des arguments
	[$type_controle, $est_actif] = explode(':', $arguments);

	// Verification des autorisations :
	if (!autoriser('modifier', 'typecontrole', $type_controle)) {
		include_spip('inc/minipres');
		echo minipres();
		exit();
	}

	// On inverse l'état courant du type de contrôle
	if (
		$type_controle
		and $est_actif
	) {
		$set = [
			'actif' => $est_actif === 'oui' ? 'non' : 'oui'
		];
		sql_updateq('spip_types_controles', $set, 'identifiant=' . sql_quote($type_controle));
	}
}
