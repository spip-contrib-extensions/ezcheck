<?php
/**
 * Ce fichier contient l'action `executer_type_controle` lancée par un utilisateur autorisé pour
 * exécuter les vérifications prévues par le type de contrôle.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Cette action permet à l'utilisateur d'exécuter, de façon sécurisée,
 * les vérifications inhérentes au type de contrôle.
 *
 * @uses type_controle_executer()
 *
 * Cette action est réservée aux utilisateurs pouvant exécuter un contrôle.
 * Elle nécessite le type de contrôle et l'id de l'auteur comme arguments.
 * Quand elle est appelée via un formulaire de choix de paramètres additionnels à fournir à la fonction de contrôle
 * elle possède un argument options supplémentaires.
 *
 * @param null|string $arguments Arguments de l'action ou null si l'action est appelée par une URL
 *
 * @return void
 */
function action_executer_type_controle_dist(?string $arguments = null) : void {
	// Sécurisation.
	// Arguments attendus :
	// - l'identifiant du type de contrôle
	// - l'auteur ou 0 si l'exécution est du au génie
	// - les options fournies par le formulaire de saisie ou vide sinon.
	// Récupération des arguments de façon sécurisée.
	if (null === $arguments) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arguments = $securiser_action();
	}
	[$type_controle, $id_auteur, $options] = explode(':', $arguments);

	// Verification des autorisations
	if (!autoriser('executer', 'typecontrole', $type_controle)) {
		include_spip('inc/minipres');
		echo minipres();
		exit();
	}

	// On lance la fonction gérant l'exécution du contrôle
	include_spip('inc/ezcheck_type_controle');
	type_controle_executer($type_controle, (int) $id_auteur, $options);
}
