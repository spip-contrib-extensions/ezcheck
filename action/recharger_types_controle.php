<?php
/**
 * Ce fichier contient l'action `recharger_types_controle` lancée par un utilisateur pour
 * recharger le fichier de configuration de chaque contrôle de façon sécurisée.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Cette action permet à l'utilisateur de recharger en base de données, de façon sécurisée,
 * les types de contrôle à partir de leur fichier YAML.
 *
 * Cette action est réservée aux utilisateurs pouvant utiliser le plugin Check Factory.
 * Elle ne nécessite aucun argument.
 *
 * @uses type_controle_charger()
 *
 * @return void
 */
function action_recharger_types_controle_dist() : void {
	// Verification des autorisations : pour recharger les types de contrôle il suffit
	// d'avoir l'autorisation minimale de Check Factory.
	if (!autoriser('ezcheck')) {
		include_spip('inc/minipres');
		echo minipres();
		exit();
	}

	// Rechargement des types de contrôle : on force le recalcul complet, c'est le but.
	include_spip('inc/ezcheck_type_controle');
	type_controle_charger(true);
}
