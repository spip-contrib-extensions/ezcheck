<?php
/**
 * Ce fichier contient l'action `cloturer_observations` lancée par un utilisateur autorisé sur un type de contrôle donné, pour :
 * - acquitter ou corriger toutes les anomalies ouvertes
 * - ou passer à l'état poubelle toutes les observations ou les anomalies fermées.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Cette action permet à l'utilisateur, de façon sécurisée, de passer à l'état poubelle
 * toutes les observations ou les anomalies fermées d'un type de contrôle ou d'acquitter ou de corriger toutes
 * les anomélies ouvertes d'un type de contrôle.
 *
 * Cette action est réservée aux utilisateurs pouvant cloturer les observations d'un type de contrôle.
 * Elle nécessite l'action requise, l'identifiant du type de contrôle, l'indicateur d'anomalie (oui/non) et l'id de l'auteur.
 *
 * @uses observation_repertorier()
 * @uses observation_cloturer()
 *
 * @param null|string $arguments Arguments de l'action ou null si l'action est appelée par une URL
 *
 * @return void
 */
function action_cloturer_observations_dist(?string $arguments = null) : void {
	// Sécurisation.
	// Arguments attendus :
	// - l'identifiant de l'action (acquitter, corriger ou supprimer)
	// - l'identifiant du type de contrôle
	// - l'indicateur d'anomalie
	// - et l'id de l'auteur
	// Récupération des arguments de façon sécurisée.
	if (null === $arguments) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arguments = $securiser_action();
	}
	[$action, $type_controle, $est_anomalie, $id_auteur] = explode(':', $arguments);

	// Verification des autorisations :
	if (!autoriser('cloturer', 'typecontrole', $type_controle, $id_auteur, ['action' => $action])) {
		include_spip('inc/minipres');
		echo minipres();
		exit();
	}

	// On recherche toutes les observations ou anomalies déjà corrigées ou acquittées et associées
	// au type de contrôle.
	include_spip('inc/ezcheck_observation');
	$filtres = [
		'type_controle' => $type_controle,
		'est_anomalie'  => $est_anomalie,
		'statut'        => $action === 'supprimer' ? 'fermee' : 'ouverte'
	];
	$ids = observation_repertorier($filtres, 'id_observation');

	// On boucle sur chaque observation et on la met à la poubelle.
	foreach ($ids as $_id_observation) {
		if (autoriser('instituer', 'observation', (int) $_id_observation, (int) $id_auteur, ['action' => $action])) {
			observation_cloturer($action, $_id_observation, (int) $id_auteur);
		}
	}
}
