<?php
/**
 * Ce fichier contient l'action `cloturer_observation` lancée par un utilisateur autorisé pour
 * corriger, acquitter ou supprimer une observation.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Cette action permet à l'utilisateur de corriger ou d'acquitter de façon sécurisée
 * une anomalie ou, de supprimer toute observation déjà fermée.
 *
 * Cette action est réservée aux utilisateurs pouvant instituer une observation.
 * Elle nécessite l'action proprement dite, à savoir, acquitter, corriger ou supprimer, l'id de l'observation et
 * celui de l'auteur.
 *
 * @uses observation_cloturer()
 *
 * @param null|string $arguments Arguments de l'action ou null si l'action est appelée par une URL
 *
 * @return void
 */
function action_cloturer_observation_dist(?string $arguments = null) : void {
	// Sécurisation.
	// Arguments attendus :
	// - le nom de l'action corriger ou acquitter
	// - l'identifiant de l'observation
	// - et l'id de l'auteur
	// Récupération des arguments de façon sécurisée.
	if (null === $arguments) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arguments = $securiser_action();
	}
	[$action, $id_observation, $id_auteur] = explode(':', $arguments);

	// Verification des autorisations
	if (!autoriser('instituer', 'observation', $id_observation, $id_auteur, ['action' => $action])) {
		include_spip('inc/minipres');
		echo minipres();
		exit();
	}

	// On lance la fonction d'acquittement ou de correction.
	include_spip('inc/ezcheck_observation');
	observation_cloturer($action, (int) $id_observation, (int) $id_auteur);
}
