<?php
/**
 * Ce fichier contient l'action `recharger_dashboards` lancée par un utilisateur pour
 * recharger le fichier de configuration de chaque dashboard de façon sécurisée.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Cette action permet à l'utilisateur de recharger dans un cache, de façon sécurisée,
 * les dashboards à partir de leur fichier JSON ou YAML.
 *
 * Cette action est réservée aux utilisateurs pouvant utiliser le plugin Check Factory.
 * Elle ne nécessite aucun argument.
 *
 * @uses dashboard_charger()
 *
 * @return void
 */
function action_recharger_dashboards_dist() : void {
	// Verification des autorisations : pour recharger les types de contrôle il suffit
	// d'avoir l'autorisation minimale d'accéder au contrôles de contrib.
	if (!autoriser('ezcheck')) {
		include_spip('inc/minipres');
		echo minipres();
		exit();
	}

	// Rechargement des dashboards.
	include_spip('inc/ezcheck_dashboard');
	dashboard_charger();
}
